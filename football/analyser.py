import scipy.ndimage
import numpy as np


def analyse_frame(frame_data):

    ball_coords = np.where((frame_data[::-1, :, 0] < 80) &
                           (frame_data[::-1, :, 1] < 80) &
                           (frame_data[::-1, :, 2] < 80))
    if len(ball_coords[0]) < 1 or len(ball_coords[1]) < 2:
        return
    else:
        # print(ball_coords)
        return ball_coords[1][0], len(frame_data) - ball_coords[0][0]


def quantise_matrix(img_matrix, resolution):
    quantised_matrix = scipy.ndimage.zoom(img_matrix, (resolution, resolution, 1), order=0)
    return quantised_matrix
