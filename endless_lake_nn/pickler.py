import pickle as pk
import time
import datetime

from endless_lake_nn import settings


def pickle(obj=None, pickle_type='dataset'):
    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H_%M_%S')
    file_name = settings.PICKLE_VERSION_PREFIX + 'endless-lake-' + timestamp + '.' + pickle_type
    path = pickle_type + '/' + file_name
    print('Pickling file...')
    pk.dump(obj, open(path, 'wb'))
    print('Pickled ' + path)


def read(path):
    return pk.load(open(path, 'rb'))

if __name__ == '__main__':
    pickle()