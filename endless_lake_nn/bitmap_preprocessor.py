import numpy as np
import scipy.ndimage


def get_game_bounds(img_matrix):
    """Given a bitmap, determines the location of the Facebook game window within the parent bitmap

    :param img_matrix: Input image matrix as a numpy array
    :return: Tuple of (x, y, width, height) of the window of the game
    """

    assert len(img_matrix) > 0
    max_width = len(img_matrix[0])
    max_height = len(img_matrix)

    # Make sure no noise around edge of window is captured
    edge_trim = 5

    # Iterate horizontally and positively scanning for RGB threshold (channel: blue)
    x = __get_threshold(img_matrix[int(max_height / 2)]) + edge_trim
    y = __get_threshold([row[int(max_width / 2)] for row in img_matrix]) + edge_trim

    width = (max_width - x) - __get_threshold(img_matrix[int(max_height / 2)], reverse=True) - edge_trim
    height = (max_height - y) - __get_threshold([row[int(max_width / 2)] for row in img_matrix], reverse=True) - edge_trim

    # plt.imshow(img_matrix, interpolation='none')
    # plt.show()

    if width * height == 0:
        print(x, y, width, height)
        raise Exception('Dimensions of capture must be greater than 0x0')

    return [x, y, width, height]


def __get_threshold(array, threshold=128, reverse=False):
    if reverse:
        array = array[::-1]

    for i, rgb_data in enumerate(array):
        # noinspection PyTypeChecker
        if np.any(rgb_data > threshold):
            return i
    return 0


def quantise_matrix(img_matrix, resolution):
    quantised_matrix = scipy.ndimage.zoom(img_matrix, (resolution, resolution, 1), order=0)
    return quantised_matrix


def get_compressed_bounds(window_bounds, compressed_bounds):
    c_x = int(window_bounds[0] + window_bounds[2] * compressed_bounds[0])
    c_y = int(window_bounds[1] + window_bounds[3] * compressed_bounds[1])
    c_width = int(window_bounds[2] * compressed_bounds[2])
    c_height = int(window_bounds[3] * compressed_bounds[3])
    return c_x, c_y, c_width, c_height
