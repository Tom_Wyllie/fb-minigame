import os
import timeit

from endless_lake_nn import pickler
from endless_lake_nn import endless_lake

import sklearn.neural_network.multilayer_perceptron as mlp
import numpy as np


def main():
    # nn_model = mlp.MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(128, 8), random_state=1, verbose=True)
    nn_model = mlp.MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(64, 16), random_state=1, verbose=True)
    game = endless_lake.Game()

    # TODO can the model be trained in chunks instead of loading the entire training set into the memory at once?
    train_input = []
    train_output = []

    read_start_time = timeit.default_timer()
    for subdir, dirs, files in os.walk('dataset'):
        for file in files:
            ext = os.path.splitext(file)[-1].lower()
            if ext in ['.dataset']:
                print('Processing ' + os.path.join(subdir, file))
                raw_train_frames = pickler.read(os.path.join(subdir, file))
                for frame in raw_train_frames:
                    frame = np.array(frame)
                    train_input.append(game.get_game_frame(frame[0]).flatten())
                    train_output.append(frame[1])

    print('Processed dataset in ' + str(timeit.default_timer() - read_start_time) + ' seconds')
    print('Training neural network on ' + str(len(train_input)) + ' frames of data...')

    start_time = timeit.default_timer()
    nn_model.fit(train_input, train_output)
    pickler.pickle(nn_model, pickle_type='model')
    print('Done in ' + str(timeit.default_timer() - start_time) + ' seconds')
    return nn_model

if __name__ == '__main__':
    main()