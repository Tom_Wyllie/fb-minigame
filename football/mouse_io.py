import timeit
import sys

from pynput import mouse

from football import settings


class MouseIO:
    def __init__(self):
        self.is_clicked = False
        self.controller = mouse.Controller()
        self.listener = mouse.Listener(on_click=self.on_click)
        self.listener.start()
        self.last_click_time = timeit.default_timer()

    def on_click(self, x, y, button, pressed):
        self.is_clicked = pressed

    def click_press(self):
        now = timeit.default_timer()

        if self.is_clicked:
            print('Exiting')
            sys.exit()

        if now > self.last_click_time + settings.MIN_CLICK_WAIT:
            print('Clicking')
            self.controller.press(mouse.Button.left)
            self.last_click_time = now

    def set_position(self, x, y):
        self.controller.position = (x, y)

    def move(self, x, y):
        self.controller.move(x, y)

    def click_release(self):
        self.controller.release(mouse.Button.left)

    def close(self):
        self.listener.join()
        mouse.Listener.stop()
