import timeit

from pynput import mouse

from endless_lake_nn import settings

class MouseIO:
    def __init__(self):
        self.is_clicked = False
        self.controller = mouse.Controller()
        self.listener = mouse.Listener(on_click=self.on_click)
        self.listener.start()
        self.last_click_time = timeit.default_timer()

    def on_click(self, x, y, button, pressed):
        self.is_clicked = pressed

    def click_press(self):
        print('Clickity click')
        now = timeit.default_timer()
        if now - self.last_click_time > settings.MIN_CLICK_WAIT:
            self.controller.press(mouse.Button.left)
            self.last_click_time = now

    def click_release(self):
        self.controller.release(mouse.Button.left)

    def is_mouse_down(self):
        return self.is_clicked

    def close(self):
        self.listener.join()
        mouse.Listener.stop()
