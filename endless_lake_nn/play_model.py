import os
import timeit
import time

from endless_lake_nn import pickler
from endless_lake_nn import settings
from endless_lake_nn import endless_lake
from endless_lake_nn import mouse_io
from endless_lake_nn import train_model

from sklearn.neural_network.multilayer_perceptron import MLPClassifier

models = []
for subdir, dirs, files in os.walk('model'):
    for file in files:
        ext = os.path.splitext(file)[-1].lower()
        if ext in ['.model']:
            models.append(os.path.join(subdir, file))

# Just go with the first one in the folder for now
nn_model = pickler.read(models[0])

start_time = timeit.default_timer()
game = endless_lake.Game()
mouse_io = mouse_io.MouseIO()

frame_time = 1.0 / settings.PLAY_FRAMERATE
last_model_pickle = timeit.default_timer()

dataset = []
while True:
    frame_start_time = timeit.default_timer()
    quantised_data = game.get_quantised_capture()

    if settings.LEARN_WHILST_PLAYING:
        if game.is_game_ended(quantised_data):
            print('Detected game end!')
            if len(dataset) > settings.MIN_SUCCESS_TIME * settings.PLAY_FRAMERATE:
                # Remove the last few seconds of recorded data, as the game fades to end screen
                dataset = dataset[:-4*settings.PLAY_FRAMERATE]
                pickler.pickle(dataset, 'dataset')

                if timeit.default_timer() - last_model_pickle > settings.MODEL_PICKLE_TIME:
                    nn_model = train_model.main()
                    last_model_pickle = timeit.default_timer()
                    del game
                    game = endless_lake.Game()
            # Restart the game
            mouse_io.click_press()
            mouse_io.click_release()
            dataset = []
            # Give the game a bit of time to load or we'll just confuse the neural net
            time.sleep(2.0)

    frame_data = game.get_game_frame(quantised_data)

    thoughts = nn_model.predict(frame_data.flatten().reshape(1, -1))
    if thoughts[0]:
        mouse_io.click_press()
    else:
        mouse_io.click_release()

    dataset.append([quantised_data, thoughts[0]])

    end_time = timeit.default_timer()
    dt = frame_time - (end_time - frame_start_time)
    if dt > 0:
        time.sleep(dt)
