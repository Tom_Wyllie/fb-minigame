# Game capture options
QUANTISATION_RESOLUTION = 0.05  # By what factor is the raw pixel data is quantised
GAME_PLAY_DURATION = 30.0
GAME_BOUNDS = (800, 350, 600, 650)
PLAY_FRAMERATE = 100
MIN_CLICK_WAIT = 0.0
