import numpy as np
import wx


class BitmapCapture:
    def __init__(self):
        self.app = wx.App()
        self.screen = wx.ScreenDC()

    def get_bitmap(self, x, y, width, height):
        bmp = wx.Bitmap(width, height)

        mem = wx.MemoryDC(bmp)
        mem.Blit(0, 0, width, height, self.screen, x, y)

        # Delete to prevent memory leakage
        del mem

        # Create a buffer object of that the bitmap will take
        data = np.zeros(3 * width * height, dtype=np.uint8)
        bmp.CopyToBuffer(data)
        del bmp

        data = np.reshape(np.array(data), [height, width, 3])
        return data
