### Game capture options ###
QUANTISATION_RESOLUTION = 0.1  # By what factor is the raw pixel data is quantised
COMPRESSED_BOUNDS = (0.05, 0.0, 0.9, 1.0)  # x, y, width, height
RELEVANT_ROW_COUNT_TOP = 5  # How many rows beneath the top of the player are we interested in
RELEVANT_ROW_COUNT_BOTTOM = 15  # How many rows beneath the top of the player are we interested in
RELEVANT_COLUMN_COUNT = 20  # Only process this number of columns either side of the player

### Data logging configuration options ###
INITIAL_DELAY = 2.5  # Seconds
GATHER_DATASET_DURATION = 12.0  # Seconds

### Pickle creation prefix to track version of data sets ###
PICKLE_VERSION_PREFIX = '2b-'

### Neural net game playing settings ###
GAME_PLAY_DURATION = 30.0
PLAY_FRAMERATE = 30
MIN_CLICK_WAIT = 0.15
LEARN_WHILST_PLAYING = True
MODEL_PICKLE_TIME = 600  # Pickle the model after every this number of seconds
MIN_SUCCESS_TIME = 20  # Minimum amount of time (including fade to end screen) for the run to be considered worth pickling and retraining on
