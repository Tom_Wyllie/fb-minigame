import timeit
import numpy as np
import ctypes
import matplotlib.pyplot as plt

from endless_lake_nn import bitmap_capture
from endless_lake_nn import bitmap_preprocessor
from endless_lake_nn import settings


class Game:
    def __init__(self):
        user32 = ctypes.windll.user32
        screen_size = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)

        self.capturer = bitmap_capture.BitmapCapture()
        full_screen_capture = self.capturer.get_bitmap(0, 0, *screen_size)
        self.game_bounds = bitmap_preprocessor.get_game_bounds(full_screen_capture)
        self.birth_time = timeit.default_timer()

    def get_quantised_capture(self):
        raw_frame_data = self.capturer.get_bitmap(*self.game_bounds)
        return bitmap_preprocessor.quantise_matrix(raw_frame_data, 0.2)

    def get_game_frame(self, quantised_matrix):

        # Get sub image

        # Quantise the dataset even further
        quantised_matrix = bitmap_preprocessor.quantise_matrix(quantised_matrix, 0.5)

        # Find player in image, this is done before classifying image to prevent error being amplified
        player_row_index, player_column_index = self.__get_player_index(quantised_matrix)

        # Filter the input data to remove noise
        frame_data = self.parse_data_from_image(quantised_matrix)

        # Vertical normalisation
        if player_row_index > (len(frame_data) - settings.RELEVANT_ROW_COUNT_BOTTOM):
            player_row_index = len(frame_data) - settings.RELEVANT_ROW_COUNT_BOTTOM
        elif player_row_index < settings.RELEVANT_ROW_COUNT_TOP:
            player_row_index = settings.RELEVANT_ROW_COUNT_TOP

        frame_data = frame_data[
                     player_row_index - settings.RELEVANT_ROW_COUNT_TOP: player_row_index + settings.RELEVANT_ROW_COUNT_BOTTOM]

        # Horizontal normalisation
        column_high = player_column_index + settings.RELEVANT_COLUMN_COUNT
        column_low = player_column_index - settings.RELEVANT_COLUMN_COUNT

        if column_high > len(frame_data[0]):
            # append extra zeros to right hand side of image
            overflow = column_high - len(frame_data[0])
            temp = np.zeros((len(frame_data), len(frame_data[0]) + overflow))
            temp[:, :-overflow] = frame_data
            frame_data = temp

        if column_low < 0:
            # append extra zeros to left hand side of image
            underflow = np.abs(column_low)
            temp = np.zeros((len(frame_data), underflow + len(frame_data[0])))
            temp[:, underflow:] = frame_data
            frame_data = temp

            # Take into account that all columns have shifted
            column_low += underflow
            column_high += underflow

        frame_data = frame_data[:, column_low: column_high]
        return np.array(frame_data, dtype=np.uint8)

    @staticmethod
    def is_game_ended(quantised_matrix):
        return np.all(quantised_matrix[180][25:30] == [64, 128, 255])

    def get_lifespan(self):
        return timeit.default_timer() - self.birth_time

    @staticmethod
    def parse_data_from_image(img_matrix):
        # Bit shift filters
        red_filter = np.floor(img_matrix[:, :, 0] / 128) * 1
        green_filter = np.floor(img_matrix[:, :, 1] / 128) * 2
        blue_filter = np.floor(
            img_matrix[:, :, 2] / 200) * 4  # Different threshold to distinguish path from white noise
        img = red_filter + green_filter + blue_filter + 2
        img[img == 5] = 1  # Path gets value 1
        img[img > 2] = 0  # Water and noise reduced to zero
        return img

    @staticmethod
    def __get_player_index(img_matrix):
        # Uses blue channel to look for player
        occurrence = np.where(img_matrix[:,:,2] < 20)
        if len(occurrence[0]) > 0:
            return int(occurrence[0][0]), int(occurrence[1][0])
        else:
            return len(img_matrix[0])/2, len(img_matrix)/2
