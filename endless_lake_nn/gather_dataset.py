import sys
import time
import timeit

from endless_lake_nn import endless_lake
from endless_lake_nn import mouse_io
from endless_lake_nn import pickler
from endless_lake_nn import settings


def gather():
    """Logs screen data and mouse strokes to create a set of data to train the neural network on

    :return: None
    """

    game = endless_lake.Game()
    mouse_listener = mouse_io.MouseIO()
    start_time = timeit.default_timer()
    dataset = []

    while timeit.default_timer() < start_time + settings.GATHER_DATASET_DURATION:
        # Append 80% quantised data to output pickle
        quantised_data = game.get_quantised_capture()
        dataset.append([quantised_data, mouse_listener.is_mouse_down()])

        # import matplotlib.pyplot as plt
        # plt.imshow(quantised_data, interpolation='none')
        # plt.show()

        print(start_time + settings.GATHER_DATASET_DURATION - timeit.default_timer())

    print(len(dataset))
    pickler.pickle(dataset)

if __name__ == '__main__':
    time.sleep(settings.INITIAL_DELAY)     # Give the user some times to switch to the game
    gather()
    sys.exit()
