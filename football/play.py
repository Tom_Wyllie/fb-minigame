import matplotlib.pyplot as plt

from football import mouse_io
from football import settings
from football import bitmap_capture
from football import analyser

import time
import timeit

frame_time = 1.0 / settings.PLAY_FRAMERATE
capturer = bitmap_capture.BitmapCapture()
mouse = mouse_io.MouseIO()

# while True:

start_time = timeit.default_timer()
for _ in range(5000):

    frame_data = capturer.get_bitmap(*settings.GAME_BOUNDS)
    quantised_frame = analyser.quantise_matrix(frame_data, settings.QUANTISATION_RESOLUTION)

    ball_index = analyser.analyse_frame(frame_data)

    if ball_index is not None:
        ball_coords = int(ball_index[0] + settings.GAME_BOUNDS[0]), \
                      int(ball_index[1] + settings.GAME_BOUNDS[1])
        mouse.set_position(*ball_coords)
        mouse.click_press()
        mouse.click_release()

        # print(ball_index)
        # print(ball_coords)

    # plt.imshow(quantised_frame)
    # plt.show()

    # time.sleep(frame_time)
print(timeit.default_timer() - start_time)
